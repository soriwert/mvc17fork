<!DOCTYPE html>
<html>
<head>
    <title>Lista de autores</title>
    <style type="text/css">
        table, tr, td, th {
            border: solid 1px black;
            padding: 0px;
            margin: 0px;
            border-collapse: true;
        }
    </style>
</head>
<body>
    <header>Encabezado <hr></header>
    <content>
        <h1>Lista de autores</h1>

        <table>
            <tr>
                <th>Id</th>
                <th>Nombre</th>
                <th>Apellidos</th>
                <th>F. Nacimiento</th>
                <th>Acciones</th>
            </tr>
            <?php foreach ($authors as $author): ?>
            <tr>
            <td><?php echo $author->id ?></td>
            <td><?php echo $author->name ?></td>
            <td><?php echo $author->surname ?></td>
            <td><?php echo $author->birthdate ?></td>
            <td>
                <a href="<?php echo "delete/$author->id" ?>">borrar</a>
                 -
                <a href="<?php
                    echo "edit/$author->id"
                ?>">editar</a>
            </td>
            </tr>
            <?php endforeach ?>
        </table>

        <a href="author/create">Nuevo</a>
    </content>
    <footer><hr>Pie de página</footer>
</body>
</html>
