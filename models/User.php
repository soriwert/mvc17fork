<?php
/**
*
*/
class User
{
    public $id;
    public $name;
    public $surname;
    public $age;
    public $email;

    // private static $db;

    function __construct()
    {

    }

    public function connect()
    {
        $dsn = 'mysql:dbname=mvc17;host=127.0.0.1';
        $usuario = 'root';
        $contraseña = 'root';

        try {
            $db = new PDO($dsn, $usuario, $contraseña, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES  \'UTF8\''));
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            // die('Conectado');
        } catch (PDOException $e) {
            die ('Falló la conexión: ' . $e->getMessage());
        }
        return $db;
    }

    public static function all()
    {
        $db = User::connect();

        $stmt = $db->query('SELECT * FROM users');
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'User');
        $results = $stmt->fetchAll();
        // echo '<pre>';
        // var_dump($results);
        // echo '</pre>';
        // exit();
        return $results;
    }

    public static function find($id)
    {
        $db = User::connect();

        $stmt = $db->query('SELECT * FROM users WHERE id=' . $id);
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'User');
        $results = $stmt->fetch();
        // echo '<pre>';
        // var_dump($results);
        // echo '</pre>';
        // exit();
        return $results;
    }

    public function save()
    {
        $db = $this->connect();
        $sql = "update users set name=:name where id=:id";
        $query = $db->prepare($sql);
        $query->bindParam(":name", $this->name);
        $query->bindParam(":id", $this->id);
        $result = $query->execute();
        // return $result;
    }
}
