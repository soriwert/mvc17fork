<?php

/**
*
*/
require('app/Model.php');

class Author extends Model
{
    public $id;

    function __construct()
    {
        # code...
    }


    public static function all()
    {
        $db = Author::connect();

        $stmt = $db->prepare("SELECT * FROM authors");
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'Author');

        $results = $stmt->fetchAll();
        // var_dump($results);
        return $results;
    }

    //hace el INSERT
    public function store()
    {
        $db = $this->connect();
        $sql = "INSERT INTO authors(name, surname, birthdate) VALUES(?, ?, ?)";
        $stmt = $db->prepare($sql);
        $stmt->bindParam(1, $this->name);
        $stmt->bindParam(2, $this->surname);
        $stmt->bindParam(3, $this->birthdate);
        $result = $stmt->execute();
        return $result;
    }

    public function find($id)
    {
        $db = Author::connect();
        $sql = "SELECT * FROM authors WHERE id = :id";
        $stmt = $db->prepare($sql);
        $stmt->bindParam('id', $id);
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'Author');
        $result = $stmt->fetch();
        return $result;
    }

    public function save()
    {
        $db = $this->connect();
        $sql = "UPDATE authors SET name=?, surname=?, birthdate=? WHERE id=?";
        $stmt = $db->prepare($sql);
        $stmt->bindParam(1, $this->name);
        $stmt->bindParam(2, $this->surname);
        $stmt->bindParam(3, $this->birthdate);
        $stmt->bindParam(4, $this->id);
        $result = $stmt->execute();
        return $result;
    }

    public function delete()
    {
        $db = $this->connect();
        $sql = "DELETE FROM authors WHERE id=?";
        $stmt = $db->prepare($sql);
        $stmt->bindParam(1, $this->id);
        $result = $stmt->execute();
        return $result;
    }
}
