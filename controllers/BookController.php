<?php

/**
*
*/
require_once("models/Book.php");

class BookController
{

    function __construct()
    {
        echo "En BookController<br>";
    }

    public function index()
    {

        // echo 1;
        $books = Book::all();
        require("views/book/index.php");
        // echo "<pre>";
        // var_dump($books);
    }

    public function create()
    {
        require("views/book/create.php");
    }

    public function store()
    {
        $book = new Book();
        $book->title = $_POST['title'];
        $book->author = $_POST['title'];
        $book->pages = $_POST['pages'];

        // echo "<pre>";
        // var_dump($book);

        $book->store();
        header('location: index');
    }

    public function edit($id)
    {
        // echo "Edición de libreo";

        //leer el libro
        $book = Book::find($id);

        var_dump($book);

        //mostrar vista con los datos previos en un formulario
    }
}
